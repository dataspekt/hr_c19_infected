# Priručna procjena prokuženosti u Hrvatskoj na temelju dobno specifične procjene IFR

Izvor podataka o broju umrlih: [Tjedno izvješće HZJZ od 25. siječnja](https://www.koronavirus.hr/UserDocsImages/Dokumenti/Tjedno%20izvje%C5%A1%C4%87e%2C%2025.1..pdf).

Procjena dobro specifične smrtnosti: [Levin, Andrew T et al. "Assessing the age specificity of infection fatality rates for COVID-19: systematic review, meta-analysis, and public policy implications"](https://pubmed.ncbi.nlm.nih.gov/33289900/)
